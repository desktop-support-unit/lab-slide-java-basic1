package day2;

public class Lab6 {
	
		public static void main(String[] args) {
		
		// for(int counter=0; counter <=10; counter++) {
		// 	System.out.println("Counter:" + counter);
		// }
			
			// ข้อ1
			int i = 1;
			
			while (i <= 10) {
				System.out.println(+ i);
				i++ ;
			}

			// ข้อ2
			int sum = 0 ;
			int counter2 = 1;
			
			while (counter2 <= 10) {
				sum += counter2;
				counter2++ ;
			}
			System.out.println(+ sum);

			//ข้อ3
			int counter3 = 1;
			
			while (counter3 <= 100) {

				if (counter3 % 12 == 0) {
					System.out.println("ตัวเลขที่หารด้วย12ลงตัว =" + counter3);
				}

				counter3++ ;
			}
			
			
			//ข้อ 4
			int[] num = {1,2,3,4,5};
			for(int c : num) {
			System.out.print(c + " ");
			}			
		}
}
