package day2;

public class Lab5 {
	public static void main(String[] args) {

		int score = 70;

		System.out.println("คะแนนของคุณ " + score + "คะแนน");

		switch (score) {
		case 80:
			System.out.println("เกรด A");
			break;
		case 70:
			System.out.println("เกรด B");
			break;
		case 60:
			System.out.println("เกรด C");
			break;
		case 50:
			System.out.println("เกรด D");
			break;
		case 40:
			System.out.println("เกรด F");
			break;
		default:
			System.out.println("เกรด E");
		}

	}
}