package day2;

public class Lab12 {

	public static void main(String[] args) {
	//String1 = ‘You and Me’ , String2 = ‘ you and me ‘
	
		String String1 = "You and Me" ;
		String String2 = " you and me " ; 
		
		//1. Equals ทดลองเปรียบเทียบ String 2 String ว่าเป็นค่าเดียวกันหรือไม่
		if (String1.equals(String2)) {
			System.out.println("ค่าเหมือนกัน");
		} else {
			System.out.println("ค่าไม่เหมือนกัน");
		}
		
		//2. ใช้คำสั่งค้นหาคำใน String และแสดงคำที่ค้นหาบนหน้าจอ
		String findWord = "Yu" ;
		if (String1.contains(findWord)) {
			System.out.println("พบคำว่า " + findWord);
		} else {
			System.out.println("ไม่พบคำว่า " + findWord);
		}
		
		//3. ใช้คำสั่งหาความยาวของ String นั้น และแสดงค่าความยาว String 

		System.out.println(String1 + " ความยาว = " + String1.length());
		System.out.println(String2 + " ความยาว = " + String2.length());
		
		//4. 	ใช้คำสั่งตัดข้อความหรือตัด String ตำแหน่งที่ 1-4 ออก
		System.out.println(String1.substring(4));
		
		//5. 	ใช้คำสั่งตัดช่องว่างของประโยค
		System.out.println("ตัดช่องว่างของประโยค=" + String2.trim());
		
		//6. 	ใช้คำสั่งเปลี่ยน String เป็นพิมพ์ใหญ่ทั้งหมด
		System.out.println("เป็นพิมพ์ใหญ่ทั้งหมด=" + String2.toUpperCase());
		
		//7. ใช้คำสั่งเปลี่ยน String2 เป็นพิมพ์ใหญ่ทั้งหมด และ ไม่มีช่องว่างซ้ายขวา ด้วยการเขียน code แค่บรรทัดเดียว (ใช้ Chaining นั่นเอง)
		System.out.println("เป็นพิมพ์ใหญ่ทั้งหมด และ ไม่มีช่องว่างซ้ายขวา=" + String2.toUpperCase().trim());
		
	}
}
